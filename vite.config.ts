import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import { defineConfig } from 'vite'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  build: {
    sourcemap: true,
    lib: {
      entry: resolve(__dirname, 'src/App.vue'),
      name: 'publish-test',
      formats: ['es', 'umd'],
      fileName: (format) => `publish-test.${format}.js`,
    },
    rollupOptions: {},
  },
})
